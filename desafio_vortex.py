T = int(input('Segundos restantes para o fim da jogada [1 - 24]: '))
Q = int(input('Quantidade de jogadores em quadra [2 - 5]: '))
X = int(input(f'Tempo de reposicionamento dos jogadores após o passe [1 - {Q - 1}]: '))
valor_repeticao = Q - X
#as possibilidades de passe vão diminuindo de 1 por 1 até chegar a esse valor, que se repete até o último segundo de jogo.
soma_passes = 0
contador_repeticoes = 0
possibilidade_passe = Q - 1
#o número de possibilidade de passe já começa com menos 1, pois não se contabiliza o jogador que está segurando a bola.
while possibilidade_passe != valor_repeticao:
    soma_passes += possibilidade_passe
    possibilidade_passe -= 1
    contador_repeticoes += 1
resultado = soma_passes + (valor_repeticao * (T - contador_repeticoes))
print(f'O resultado do somatório de jogadores livres para receber um passe durante {T} segundos foi: {resultado}.')